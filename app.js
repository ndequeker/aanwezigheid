const dateListElement = document.getElementById('dates')
const dateOptions = { weekday: 'short', month: 'long', day: 'numeric' }

const db = new PouchDB('data')
const remoteCouch = 'https://7186ca6e-25fb-4cc1-aef7-79329495d80e-bluemix.cloudantnosqldb.appdomain.cloud/data'

function sync() {
  const opts = { live: true }
  db.replicate.to(remoteCouch, opts, syncError)
  db.replicate.from(remoteCouch, opts, syncError)
}

function syncError(error) {
  console.error('sync error: ', error);
}

sync()

db
  .changes({
    since: 'now',
    live: true,
    include_docs: true
  })
  .on('change', change => {
    const doc = change.doc
    setCheckboxValueForDate(doc._id, doc.present)
  })

db
  .allDocs({
    include_docs: true
  })
  .then(response => {
    response.rows
      .forEach(row => {
        setCheckboxValueForDate(row.doc._id, row.doc.present)
      })
  })
  .catch(error => {
    console.error(error);
  })

document
  .querySelector('body')
  .addEventListener('click', function (event) {
    const clickedElement = event.target

    if (clickedElement.tagName.toLowerCase() === 'input') {
      const id = clickedElement.name
      const present = clickedElement.checked

      db
        .get(id)
        .catch(error => {
          if (error.name === 'not_found') {
            return { _id: id }
          }
        })
        .then(function (doc) {
          doc.present = present
          return db.put(doc);
        })
    }
  })

function setCheckboxValueForDate(inputName, checked) {
  document
    .querySelectorAll(`input[name="${inputName}"]`)
    .forEach(element => {
      element.checked = checked
    })
}

function getTemplate(templateId) {
  const template = document.querySelector(`#${templateId}`).content
  return template.cloneNode(true)
}

const dateIsWeekday = date =>
  date.getDay() > 0 && date.getDay() < 6

function generateListOfDateObjects(startDate, amount) {
  const dateList = []

  for (let i = 0; i <= amount; i++) {
    const futureDate = new Date()
    futureDate.setDate(startDate.getDate() + i)
    dateList.push(futureDate)
  }

  return dateList
}

const createDateInfoElement = (dayOfTheWeek, title, checkboxNamePrefix) => {
  const dateInfoTemplate = getTemplate('date-info-template')
  dateInfoTemplate.querySelector('.dateInfo').dataset.dayOfTheWeek = dayOfTheWeek
  dateInfoTemplate.querySelector('.dateInfo__title').textContent = title

  dateInfoTemplate
    .querySelectorAll('li[data-member-id]')
    .forEach(liElement => {
      const memberId = liElement.dataset.memberId
      liElement.querySelector('input').name = `${checkboxNamePrefix}-${memberId}`
      liElement.querySelector('label').insertAdjacentHTML( 'beforeend', `${memberId}`)
    })

  return dateInfoTemplate
}

const isLastWorkingDayOfTheWeek = date =>
  date.getDay() === 5

generateListOfDateObjects(new Date(), 30)
  .filter(date => dateIsWeekday(date))
  .map(futureDate => {
    const title = `${futureDate.toLocaleDateString('nl-nl', dateOptions)}`

    return {
      date: futureDate,
      template: createDateInfoElement(futureDate.getDay(), title, futureDate.toISOString().substr(0, 10))
    }
  })
  .forEach((result, index, array) => {
    dateListElement.appendChild(result.template)

    if (isLastWorkingDayOfTheWeek(result.date) && index !== array.length - 1) {
      dateListElement.appendChild(document.createElement('hr'))
    }
  })
